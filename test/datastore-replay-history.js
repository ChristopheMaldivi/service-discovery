const should = require('should');
const assert = require('assert');
const request = require('supertest');
const app = require('../app');
const datastore = require('../datastore/datastore');
const eventstore = require('../datastore/eventstore');

describe('History replay', function() {
  const url = 'http://localhost:3000';
  var server;

  before(function(done) {
    server = app.listen(3000);
    app.disableEventsHistory = true;
    app.on('connection', done);
    done();
  });

  /** @return Promise, to be notified when rest is done */
  const unitTestDatastoresReset = () => {
    datastore.removeAll();
    return eventstore.unitTestReset();
  };

  after(function () {
    server.close();
  });

  describe('Replay stored events to rebuild previous in memory state', function() {

    const nodeUpdateCommand1 = {
      name: "test-node-1",
      ip: "192.168.1.2",
      threads: 4
    };
    const nodeUpdateCommand2 = {
      name: "test-node-2",
      ip: "192.168.1.3",
      threads: 6
    };

    it('Send commands, then reload from history and check we recover state', function (done) {

      setTimeout(() =>

        // let mongo get nodes update asynchronously (wait 1 second)
        // for previous tests

      unitTestDatastoresReset().then( () =>
        // send update for node 1
        request(url)
          .post('/node-update')
          .type('json')
          .send(JSON.stringify(nodeUpdateCommand1))
          .expect(200)
          .then(() =>

            // send update for node 2
            request(url)
              .post('/node-update')
              .type('json')
              .send(JSON.stringify(nodeUpdateCommand2))
              .expect(200)
              .then(() => {

                  // clear in memory datastore then check no nodes are available
                  datastore.removeAll();
                  request(url)
                    .get('/available-nodes')
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .expect(200, [])
                    .then(() => {

                        setTimeout(() =>

                          // let mongo get nodes update asynchronously (wait 1 second)
                          // then replay history and check in memory datastore contains
                          // node 1 & 2 updates
                          eventstore.replayHistory(datastore, () =>

                            request(url)
                              .get('/available-nodes')
                              .set('Accept', 'application/json')
                              .expect('Content-Type', /json/)
                              .expect(200, [
                                {
                                  name: nodeUpdateCommand1.name,
                                  ip: nodeUpdateCommand1.ip,
                                  threads: nodeUpdateCommand1.threads
                                },
                                {
                                  name: nodeUpdateCommand2.name,
                                  ip: nodeUpdateCommand2.ip,
                                  threads: nodeUpdateCommand2.threads
                                },
                              ], done)
                          ), 700)

                      }
                    )
                }
              )
          )
      ), 700)
    });
  });
});