const should = require('should');
const assert = require('assert');
const request = require('supertest');
const app = require('../app');
const datastore = require('../datastore/datastore');
const eventstore = require('../datastore/eventstore');

describe('Service node update', function() {
  const url = 'http://localhost:3000';
  var server;

  before(function(done) {
    server = app.listen(3000);
    app.disableEventsHistory = true;
    app.on('connection', done);
    done();
  });

  after(function () {
    server.close();
  });

  /** @return Promise, to be notified when rest is done */
  const unitTestDatastoresReset = () => {
    datastore.removeAll();
    return eventstore.unitTestReset();
  };

  describe('Receive a Node update command', function() {

    const nodeUpdateCommand = {
      name: "test-node",
      ip: "192.168.1.2",
      threads: 4
    };

    it('should return ok http status when posting update', function (done) {
      unitTestDatastoresReset()
        .then(() => {
          request(url)
            .post('/node-update')
            .type('json')
            .send(JSON.stringify(nodeUpdateCommand))
            .expect(200, {}, done);
        });
    });

    it('available nodes contains node N after posting an update for it', function (done) {
      eventstore.unitTestReset()
        .then(() => {
          request(url)
            .post('/node-update')
            .type('json')
            .send(JSON.stringify(nodeUpdateCommand))
            .expect(200)
            .end(() =>
              request(url)
                .get('/available-nodes')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200, [{
                  name: nodeUpdateCommand.name,
                  ip: nodeUpdateCommand.ip,
                  threads: nodeUpdateCommand.threads
                }], done)
            )
        });
    });
  });
});