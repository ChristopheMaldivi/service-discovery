const should = require('should');
const assert = require('assert');
const request = require('supertest');
const app = require('../app');
const datastore = require('../datastore/datastore');
const eventstore = require('../datastore/eventstore');
const timeToLive = require('../command/timetolive');

describe('Nodes availability expires after a given time to live period', function() {
  const url = 'http://localhost:3000';
  var server;

  before(function(done) {
    server = app.listen(3000);
    app.disableEventsHistory = true;
    app.on('connection', done);
    done();
  });

  after(function () {
    server.close();
  });

  describe('Receive a Node update command, and check it expires after time to live period', function() {
    const TEST_TIMEOUT = 500;
    const nodeUpdateCommand = {
      name: "test-node",
      ip: "192.168.1.2",
      threads: 4,
      available: true
    };

    it('node availability status expires', function (done) {

      timeToLive.setTimeoutForUnitTest(TEST_TIMEOUT);

      datastore.removeAll();
      eventstore.unitTestReset()
        .then(() => {
          request(url)
            .post('/node-update')
            .type('json')
            .send(JSON.stringify(nodeUpdateCommand))
            .expect(200)
            .then(() =>
              setTimeout(() =>
                request(url)
                  .get('/available-nodes')
                  .set('Accept', 'application/json')
                  .expect('Content-Type', /json/)
                  .expect(200, [], done),
                TEST_TIMEOUT * 2)
          )
        });
    });
  });
});