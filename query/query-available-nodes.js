const datastore = require('../datastore/datastore')

class AvailableNodesQuery {

  availableNodes() {
    const nodes = datastore.availableNodes();
    for (var i = 0; i < datastore.availableNodes().length; i++) {
      delete nodes[i].updateTime;
    }
    return nodes;
  }
}

module.exports = new AvailableNodesQuery();