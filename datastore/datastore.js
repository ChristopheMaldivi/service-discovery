class Datastore {

  constructor() {
    this.datastore = new Map();
  }

  availableNodes() {
    return Array.from(this.datastore.values());
  }

  put(nodeUpdateCommand) {
    const ip = nodeUpdateCommand.ip;
    nodeUpdateCommand.updateTime = new Date().getTime();
    this.datastore.set(ip, nodeUpdateCommand);
  }

  remove(node) {
    this.datastore.delete(node.ip);
  }

  removeAll() {
    this.datastore.clear();
  }
}

module.exports = new Datastore();
