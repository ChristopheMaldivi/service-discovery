const mongoRepository = require('./mongo-repository');

class Eventstore {

  constructor() {
    this.DOC_COLLECTION = "nodes";
    mongoRepository.connect();
  }

  reloadEventsAtStartup(datastore) {
    this.replayHistory(datastore);
  }

  allEvents(callback) {
    const excludeFields = { _id: 0};
    mongoRepository.find(this.DOC_COLLECTION, {}, excludeFields, callback);
  }

  put(nodeUpdateCommand) {
    const ip = nodeUpdateCommand.ip;
    mongoRepository.upsert(this.DOC_COLLECTION, { ip: ip}, nodeUpdateCommand);
  }

  replayHistory(datastore, doneCallback) {
    this.allEvents((events) => {
        for (var i = 0; i < events.length; i++) {
          datastore.put(events[i], true);
        }
        if (doneCallback) {
          doneCallback();
        }
      }
    );
  }

  /** @return {Promise} returns Promise to know when drop is really done **/
  unitTestReset() {
    return mongoRepository.dropDatabase();
  }
}

module.exports = new Eventstore();
