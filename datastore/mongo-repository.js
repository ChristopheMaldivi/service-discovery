const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const l = require('winston');

class MongoRepository {

  constructor() {
    this.TAG = "[MongoRepository]"
    this.MONGO_URL = 'mongodb://localhost:27017/service-discovery';
  }

  find(collectionName, filter, excludeFields, callback) {
    if (this.db === undefined || this.db === null) {
      l.warn(this.TAG, "not connected to mongo, try again");
      this.connect();
      return;
    }

    let collection = this.db.collection(collectionName);
    collection.find(filter, excludeFields).toArray((err, docs) => {
      if (err !== null) {
        l.error(that.TAG, "FAILED to find docs: " + err);
      }
      callback(docs);
    });
  }

  upsert(collectionName, filter, dat) {
    if (this.db === undefined || this.db === null) {
      l.warn(this.TAG, "not connected to mongo, try again");
      this.connect();
      return;
    }

    const that = this;
    const collection = this.db.collection(collectionName);
    collection.updateOne(filter, dat, { upsert: true }, (err, r) => {
      if (err !== null) {
        l.error(that.TAG, "FAILED to update dat: " + JSON.stringify(dat));
        return;
      }
      if (r.matchedCount >= 1) {
        l.warn(that.TAG, "WARNING updated ", r.matchedCount, " items for dat: ",  JSON.stringify(dat));
      }
      l.info(that.TAG, "Update, dat: " + JSON.stringify(dat));
    });
  }

  /** @return {Promise} returns Promise to know when drop is really done **/
  dropDatabase() {
    if (this.db) {
      return this.db.dropDatabase();
    }
  }

  connect() {
    if (this.db !== undefined && this.db !== null) {
      return;
    }

    let that = this;
    MongoClient.connect(this.MONGO_URL, (err, db) => {
      if (err !== null) {
        l.error(that.TAG, "FAILED to connect: " + err);
        return;
      }
      l.info(that.TAG, "Connected to server");
      this.db = db;
    });
  }
};

module.exports = new MongoRepository();