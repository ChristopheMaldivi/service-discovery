const express = require('express');
const router = express.Router();
const l = require('winston');
const nodeUpdateCommandHandler = require('../command/cmd-node-update');

router.post('/', (req, res) => {
  const nodeUpdateCommand = req.body;
  l.info("node-update", JSON.stringify(nodeUpdateCommand));
  nodeUpdateCommandHandler.handle(nodeUpdateCommand);
  res.sendStatus(200);
});

module.exports = router;
