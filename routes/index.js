const express = require('express');
const router = express.Router();
const availableServicesQuery = require('../query/query-available-nodes');

router.get('/', (req, res) => {
    //const nodes = availableServicesQuery.availableNodes();
    var nodes = new Array(2);
    const nodeUpdateCommand1 = {
      name: "test-node-1",
      ip: "192.168.1.2",
      threads: 4
    };
    const nodeUpdateCommand2 = {
      name: "test-node-2",
      ip: "192.168.1.3",
      threads: 6
    };
    nodes[0] = nodeUpdateCommand1;
    nodes[1] = nodeUpdateCommand2;
    res.render('index', {title: 'Available Nodes', nodes: nodes,});
  }
);

module.exports = router;
