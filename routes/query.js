const express = require('express');
const router = express.Router();

const availableServicesQuery = require('../query/query-available-nodes');

router.get('/', (req, res) => {
  const nodes = availableServicesQuery.availableNodes();
  if (nodes === undefined) {
    res.status(500).json({error: "FAILED to retrieve available nodes"})
    return;
  }
  res.json(nodes);
});

module.exports = router;
