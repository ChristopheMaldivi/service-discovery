const datastore = require('../datastore/datastore')
const l = require('winston');

class TimeToLive {

  constructor() {
    this.tag = "TimeToLive";
    this.timeout = 30 * 1000;
    this.setupTimeout();
  }

  setupTimeout() {
    setInterval(this.buildTimeoutCallback(), this.timeout);
  }

  buildTimeoutCallback() {
    const that = this;
    return function() {
      that.checkTimeout(that);
    };
  }

  checkTimeout(that) {
    const nodes = datastore.availableNodes();
    const currentTime = new Date().getTime();
    for (var i = 0; i < nodes.length; i++) {
      let node = nodes[i];
      const elapsedTimeSinceUpdate = currentTime - node.updateTime;
      if (elapsedTimeSinceUpdate > that.timeout) {
        datastore.remove(node);
        l.info(this.tag, "remove node: " + JSON.stringify(node));
      }
    }
  }

  setTimeoutForUnitTest(timeout) {
    this.timeout = timeout;
    this.setupTimeout();
  }
}

module.exports = new TimeToLive();
