const datastore = require('../datastore/datastore')
const eventstore = require('../datastore/eventstore')

class NodeUpdateCommandHandler {

  handle(nodeUpdateCommand) {
    datastore.put(nodeUpdateCommand);
    eventstore.put(nodeUpdateCommand);
  }
}

module.exports = new NodeUpdateCommandHandler();